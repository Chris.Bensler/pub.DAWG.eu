-- Preprocessed using EUPP created by Chris Bensler

---------------------------------------------------------------------------------
-- dawg.e
-- version 0.3a
-- DLL Automatic Wrapper Generator
-- written by Chris Bensler
-- LAST MODIFIED : 03/27/02 / 01:53
--
--the only global routine is build_wrapper()
---------------------------------------------------------------------------------
include rds/file.e
include rds/misc.e
include sequence.e
include dll_wrap.e

with type_check
include dawg_err.e

constant dawg_version = "0.3a"

---------------------------------------------------------------------------------
-- LOCAL TYPE DEFINITIONS --

function is_alias(object x)
   return sequence(x[1][1])
end function

function alias_list(sequence s, integer args)
 sequence list
   if length(s) = 1 or length(s) = 2 then
      if not (length(s[1]) and byte_array(s[1])) then return 0 end if
      if length(s) = 2 then
         if length(s[2]) != args then return 0 end if
         list = s[2]
         for i = 1 to length(s[2]) do
            if not (length(s[2][i]) and byte_array(s[2][i])) then return 0 end if
            list = splice(list,{1,1},"")
            if find(s[2][i],list) then return 0 end if
         end for
      end if
   end if
   return 1
end function

-- {"Name",{vars},ret} -- len = 3 : [1][1] = 'N'
-- {
--    {"Name",{vars},ret}, -- len = 3 : [1][1][1] = 'N'
--    {"Alias",{aliases}} -- aliases
-- }
type c_func_type(object x)
   if sequence(x) and length(x) and length(x[1]) then
      if is_alias(x) then
         if length(x[1]) !=3 then return 0 end if
         if not alias_list(x[2],length(x[1][2])) then return 0 end if
         x = x[1]
      end if
      if length(x) = 3 then
         if byte_array(x[1]) and sequence(x[2]) and object(x[3]) then
            return 1
         end if
      end if
   end if
   return 0
end type

-- {"Name",{vars}} -- len = 2 : [1][1] = 'N'
-- {
--    {"Name",{vars}}, -- len = 2 : [1][1][1] = 'N'
--    {"Alias",{aliases}} -- aliases
-- }
type c_proc_type(object x)
   if sequence(x) and length(x) and length(x[1]) then
      if is_alias(x) then
         if length(x[1]) !=2 then return 0 end if
         if not alias_list(x[2],length(x[1][2])) then return 0 end if
         x = x[1]
      end if
      if length(x) = 2 then
         if sequence(x[1]) and sequence(x[2]) then
            return 1
         end if
      end if
   end if
   return 0
end type

-- {"Name"} -- len = 1 : [1][1] = 'N'
-- {
--    {"Name"}, -- len = 1 : [1][1][1] = 'N'
--    {"Alias"} -- alias
-- }
type c_var_type(object x)
   if sequence(x) and length(x) and length(x[1]) then
      if is_alias(x) then
         if length(x[1]) !=1 then return 0 end if
         if not alias_list(x[2],0) then return 0 end if
         x = x[1]
      end if
      if length(x) = 1 then
         if sequence(x[1]) then
            return 1
         end if
      end if
   end if
   return 0
end type

---------------------------------------------------------------------------------
-- LOCAL BUILD_WRAPPER PARSING ROUTINES --

constant c_type_list ={{C_CHAR,C_UCHAR,C_SHORT,C_USHORT,C_LONG,C_POINTER
      ,C_FLOAT,C_DOUBLE,E_INTEGER,E_ATOM,E_SEQUENCE,E_OBJECT},{"C_CHAR","C_UCHAR","C_SHORT"
      ,"C_USHORT","C_LONG","C_POINTER","C_FLOAT","C_DOUBLE"
      ,"E_INTEGER","E_ATOM","E_SEQUENCE","E_OBJECT"}}

constant NAME =1,ARGS =2,RESULT =3

sequence DLL_Names      DLL_Names ={}
sequence DLL_Links      DLL_Links ={}
sequence Var_Names      Var_Names ={}
sequence Var_Aliases    Var_Aliases ={}
sequence Var_Links      Var_Links ={}
sequence Func_Names     Func_Names ={}
sequence Func_Aliases   Func_Aliases={}
sequence Func_Links     Func_Links ={}
sequence Func_Wraps     Func_Wraps ={}
sequence Proc_Names     Proc_Names ={}
sequence Proc_Aliases   Proc_Aliases ={}
sequence Proc_Links     Proc_Links ={}
sequence Proc_Wraps     Proc_Wraps ={}

function combine(sequence s)
   if length(s) then
      if length(s) >1 then
         for i =2 to length(s) do
            s[1] &=s[i]
         end for
      end if
      s =s[1]
   end if
   return s
end function

function wrap_line(sequence line,integer len)
 sequence tmp,tmp2
 integer f,f2
   tmp ={}
   tmp2 =""
   while length(tmp2)+find('\n',line) >len do
      f =find(',',line)
      f2 =find(' ',line)
      if f2 <f then f =f2 end if
      if f =0 then exit end if
      if length(tmp2)+f >len then
         tmp &=tmp2 & "\n"
         tmp2 ="         "
      else
         tmp2 &=line[1..f]
         line =line[f+1..length(line)]
      end if
   end while
   return tmp & tmp2 & line
end function

function c_types_2_string(sequence s, sequence link_item)
 integer found
   for i =1 to length(s) do
      if sequence(s[i]) then
         s[i] =C_POINTER
      end if
      found =find(s[i],c_type_list[1])
      on_exception({found,link_item},ex_c_types_2_string_ID)
      if found then
         s[i] =c_type_list[2][found] & ","
      end if
   end for
   s =combine(s)
   if length(s) then
      s =s[1..length(s)-1]
   end if
   return s
end function

function c_types_2_args(sequence s, sequence aliases)
   for i =1 to length(s) do
      if sequence(s[i]) then
         s[i] ="object"
      elsif s[i] < C_LONG then
         s[i] ="integer"
      else
         s[i] ="atom"
      end if
      s[i] &= " "& aliases[i] & ", "
   end for
   s =combine(s)
   if length(s) then
      s =s[1..length(s)-2]
   end if
   return s
end function

procedure add_fwrap(sequence func, sequence aliases)
 sequence wrap
   --[1] function declare
   wrap ={"global function " & aliases[1] & "(" & c_types_2_args(func[ARGS],aliases[2]) & ")\n",
      " atom ret\n",    --[2] retptr declare
      "",      --[3] strptr tmp's
      "",    --[4] strptr allocations
      "   ret = c_func(" & aliases[1] & "_,{",    --[5] c_func()
      "",                     --[6] strptr free's
      "   return ",           --[7] return
      "end function\n\n"      --[8] end function
   }
   if length(func[ARGS]) then
      for i =1 to length(func[ARGS]) do
         if sequence(func[ARGS][i]) then
            wrap[3] &="tmp_"&aliases[2][i]&","
            wrap[4] &="   if sequence("&aliases[2][i]&") then tmp_"&aliases[2][i]&
                        " = allocate_string("&aliases[2][i]&") else tmp_"&aliases[2][i]&
                        " = "&aliases[2][i]&
                  " end if\n"
            wrap[5] &="tmp_"&aliases[2][i]&","
            wrap[6] &="   if sequence("&aliases[2][i]&") then free(tmp_"&aliases[2][i]&
                     ") end if\n"
         else
            wrap[5] &=aliases[2][i]&","
         end if
      end for
      if length(wrap[3]) then
         wrap[3] = " atom "&wrap[3][1..length(wrap[3])-1]
      end if
      if length(wrap[4]) then
         wrap[4] ="\n"&wrap[4]&"\n"
         wrap[6] ="\n"&wrap[6]&"\n"
      end if
      if length(func[ARGS]) then
         wrap[5] =wrap[5][1..length(wrap[5])-1]
      end if   
   end if
   wrap[5] &="})\n"
   if length(wrap[4]) then
      if sequence(func[RESULT]) then
         wrap[7] &="peek_string(ret)\n"
      else
         wrap[7] &="ret\n"
      end if
   else
      wrap[2] =""       -- don't need it
      wrap[7] =""       -- don't need it

      wrap[5] =wrap[5][10..length(wrap[5])-1]
      if sequence(func[RESULT]) then
         wrap[5] ="peek_string(" & wrap[5] & ")"
      end if
      wrap[5] ="   return " & wrap[5] & "\n"
   end if
   Func_Wraps &={combine(wrap)}
end procedure

procedure add_pwrap(sequence proc, sequence aliases)
 sequence wrap
   wrap ={"global procedure " &aliases[1]& "(" &c_types_2_args(proc[ARGS],aliases[2])& ")\n",    --[1] procedure declare
      "",    --[2] strptr tmp's
      "",    --[3] strptr allocations
      "   c_proc(" &aliases[1]& "_,{",    --[4] c_proc()
      "",                     --[5] strptr free's
      "end procedure\n\n"      --[6] end procedure
}
   if length(proc[ARGS]) then
      for i =1 to length(proc[ARGS]) do
         if sequence(proc[ARGS][i]) then
            wrap[2] &="tmp_" &aliases[2][i]& ","
            wrap[3] &="   if sequence(" &aliases[2][i]& ") then tmp_" &aliases[2][i]&
                           " = allocate_string(" &aliases[2][i]& ") else tmp_" &aliases[2][i]&
                           " = " &aliases[2][i]& " end if\n"
            wrap[4] &="tmp_" &aliases[2][i]& ","
            wrap[5] &="   if sequence(" &aliases[2][i]& ") then free(tmp_" &aliases[2][i]&
                     ") end if\n"
         else
            wrap[4] &=aliases[2][i]& ","
         end if
      end for
      if length(wrap[2]) then
         wrap[2] = " atom "&wrap[2][1..length(wrap[2])-1]
      end if
      if length(wrap[3]) then
         wrap[3] ="\n"&wrap[3][1..length(wrap[3])-1] & "\n\n"  
         wrap[5] ="\n"&wrap[5]
      end if
      if length(proc[ARGS]) then
         wrap[4] =wrap[4][1..length(wrap[4])-1]
      end if
   end if
   wrap[4] &="})\n"
   Proc_Wraps &={combine(wrap)}
end procedure

procedure add_func(sequence dll_name,sequence link, sequence link_item)
 sequence func, aliases
   if is_alias(link) then
      func = link[1]
      aliases = link[2]
   else
      func = link
      aliases ={ func[NAME] }
   end if
   if length(aliases) != 2 then
      aliases &={ {} }
      for i = 1 to length(func[ARGS]) do
         aliases[2] &={ sprintf("arg%d",i) }
      end for
   end if

   on_exception({Func_Names,func[NAME],link_item},ex_double_ID)
   on_exception({Func_Aliases,aliases[1],link_item},ex_alias_ID)
   if not catch_exception(ex_double_ID,link_item) then
      if not catch_exception(ex_alias_ID,link_item) then

         Func_Names &={func[NAME]}
         Func_Links &={"global constant " & aliases[1] & "_ = link_c_func(" & dll_name & 
               ", " & "\"" & func[NAME] & "\", " & "{" & c_types_2_string(func[ARGS],link_item)
                & "}, " & c_types_2_string({func[RESULT]},link_item) & ")\n"}
         if not catch_exception(ex_c_types_2_string_ID,link_item) then
            add_fwrap(func,aliases)
         end if

      end if
   end if
end procedure

procedure add_proc(sequence dll_name,sequence link, sequence link_item)
 sequence proc, aliases
   if is_alias(link) then
      proc = link[1]
      aliases = link[2]
   else
      proc = link
      aliases ={ proc[NAME] }
   end if
   if length(aliases) != 2 then
      aliases &={ {} }
      for i = 1 to length(proc[ARGS]) do
         aliases[2] &={ sprintf("arg%d",i) }
      end for
   end if

   on_exception({Proc_Names,proc[NAME],link_item},ex_double_ID)
   on_exception({Proc_Aliases,aliases[1],link_item},ex_alias_ID)
   if not catch_exception(ex_double_ID,link_item) then
      if not catch_exception(ex_alias_ID,link_item) then

         Proc_Names &={proc[NAME]}
         Proc_Links &={"global constant " & aliases[1] & "_ = link_c_proc(" & dll_name & 
               ", " & "\"" & proc[NAME] & "\", " & "{" & c_types_2_string(proc[2],link_item) & "}" & ")\n"}
         if not catch_exception(ex_c_types_2_string_ID,link_item) then
            add_pwrap(proc, aliases)
         end if

      end if
   end if
end procedure

procedure add_var(sequence dll_name,sequence link, sequence link_item)
 sequence var, aliases
   if is_alias(link) then
      var = link[1]
      aliases = link[2]
   else
      var = link
      aliases ={ var[NAME] & "_var" }
   end if

   on_exception({Var_Names,var[NAME],link_item},ex_double_ID)
   on_exception({Var_Aliases,aliases[1],link_item},ex_alias_ID)
   if not catch_exception(ex_double_ID,link_item) then
      if not catch_exception(ex_alias_ID,link_item) then

         Var_Names &={var[NAME]}
         Var_Links &={"global constant " & aliases[1] & " = link_c_var(" & dll_name & 
               ", " & "\"" & var[NAME] & "\"" & ")\n"}

      end if
   end if
end procedure

function add_dll(sequence dll_name, integer link_item)
 sequence dll_alias
   dll_alias = dll_name

   on_exception({dll_name,link_item}, ex_dll_ID)
   if not catch_exception(ex_dll_ID,link_item) then

      if platform() = LINUX then
         dll_alias[length(dll_alias)-2] = '_'
      else
         dll_alias[length(dll_alias)-3] = '_'
      end if
      if not find(dll_name,DLL_Names) then
         DLL_Names &={dll_name}
         DLL_Links &={"global constant " & dll_alias & " = link_dll(\"" & dll_name & "\")\n"}
      end if

   end if
   return dll_alias
end function

procedure add_link(sequence dll_alias, sequence link, sequence link_item)
 integer err_flag
   err_flag = 0
   if c_func_type(link) then
      add_func(dll_alias, link, link_item)
   elsif c_proc_type(link) then
      add_proc(dll_alias, link, link_item)
   elsif c_var_type(link) then
      add_var(dll_alias, link, link_item)
   else
      err_flag = 1
   end if
   on_exception({err_flag,link_item}, ex_link_ID)
end procedure

---------------------------------------------------------------------------------
-- GENERATE OUTPUT AND WRITE TO FILE --

procedure output_wrapper(sequence wrapper_name)
 sequence output
 integer fn      -- generate output

   output ={
      "-- wrapper file generated using dawg.e ver"&dawg_version&"\n",
      "------------------------------------------------------------\n",
      "include dll_wrap.e\n",
      "LINK_COMPLAIN=1\n\n",
      "------------------------------------------------------------\n",
      "-- LINKING --\n"
   }
   output &={"\n-- link to the DLL's\n"}
   for i =1 to length(DLL_Links) do
      output &={DLL_Links[i]}
   end for
   if length(Var_Links) then
      output &={"\n-- link to the c_var's\n"}
      for i =1 to length(Var_Links) do
         output &={Var_Links[i]}
      end for
   end if
   if length(Func_Links) then
      output &={"\n-- link to the c_func's\n"}
      for i =1 to length(Func_Links) do
         output &={Func_Links[i]}
      end for
   end if
   if length(Proc_Links) then
      output &={"\n-- link to the c_proc's\n"}
      for i =1 to length(Proc_Links) do
         output &={Proc_Links[i]}
      end for
   end if


   output &={"\n------------------------------------------------------------\n"}
   output &={"-- WRAPPER ROUTINES --\n"}

   if length(Func_Wraps) then
      output &={"\n-----------------------------\n-- c_func wrappers --\n\n"}
      for i =1 to length(Func_Wraps) do
         output &={Func_Wraps[i]}
      end for
   end if
   if length(Proc_Wraps) then
      output &={"\n-----------------------------\n-- c_proc wrappers --\n\n"}
      for i =1 to length(Proc_Wraps) do
         output &={Proc_Wraps[i]}
      end for
   end if

   -- write it to file
   fn =open(wrapper_name,"w")
   on_exception({fn,wrapper_name}, ex_file_ID)
   if not catch_exception(ex_file_ID,wrapper_name) then
      for i =1 to length(output) do
         puts(fn,wrap_line(output[i],80))
      end for

      close(fn)
   end if
end procedure

---------------------------------------------------------------------------------
-- GLOBAL BUILD_WRAPPER ROUTINE --
-- builds an include file which wraps all items in 'build_list'
-- links all specified dll's, C functions, C procedures, and C variables
--
-- 'wrapper_name' is the name of the include file to be created.
-- 'build_list' is of the form:
--    {                    -- define build_list, each element is a dll_link_list
--       {                 -- define link_list -- length = 2
--          "Dll_Name.dll", -- length >= 5 (must find at least "x.dll")
--          {              -- define routines -- can be any length
--             {"C_Function_Name", {Args}, RetVal}, -- length = 3
--             {"C_Procedure_Name", {Args}}, -- length = 2
--             {"C_Variable_Name"} -- length = 1
--          }
--       }
--    }
--
-- each link_item (C_Func,C_Proc, C_Var) can be of either form:
--    {"Name",{vars},ret} -- simple link definition
-- or:
--    {
--       {"Name",{vars},ret}, -- definition
--       {"Alias",{var_aliases}} -- aliases
--    }
--
-- if 'wrapper_name' already exists, and 'overwrite' is FALSE,
--          the program will abort with an error msg, otherwise the file will be replaced
-- 

global function build_wrapper(sequence wrapper_name,sequence build_list,integer overwrite)
 sequence dll_alias
 integer pending
   -- check if file already exists
   on_exception({wrapper_name,overwrite},ex_wrapper_ID)

   for i =1 to length(build_list) do
      on_exception( {build_list[i],i}, ex_link_list_ID)
      if not catch_exception(ex_link_list_ID, i) then

         dll_alias = add_dll(build_list[i][1], i)
   
         for j =1 to length(build_list[i][2]) do
            add_link(dll_alias, build_list[i][2][j], {i,j})
         end for

      end if
   end for

   pending = pending_exceptions(0)
   if not pending then
      output_wrapper(wrapper_name)
      pending = pending_exceptions(0)
   end if
   process_exceptions(wrapper_name)
   clear_pending_exceptions(0)

   DLL_Names ={}
   DLL_Links ={}
   Var_Names ={}
   Var_Aliases ={}
   Var_Links ={}
   Func_Names ={}
   Func_Aliases ={}
   Func_Links ={}
   Func_Wraps ={}
   Proc_Names ={}
   Proc_Aliases ={}
   Proc_Links ={}
   Proc_Wraps ={}

   return not pending
end function

without type_check

---------------------------------------------------------------------------------
-- HISTORY --

-- v0.3 --
---------------------
-- fixed compatability with linux. always looked for ".dll" extension

-- added support for aliasing

-- integrated exception handling

-- all exceptions logged to dawg.err
--  persistent processing
--  (whole list will be processed, even if an exception is encountered)