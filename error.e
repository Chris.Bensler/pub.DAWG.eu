include rds/machine.e
include rds/misc.e
include sequence.e

with trace

---------------------------------------------------------------------------------
-- WAIT ROUTINES --

-- display msg and wait for a keypress
global procedure wait(sequence msg)
   if length(msg) then
      puts(1,msg)
   end if
   while get_key()=-1 do end while
end procedure

-- display msg, wait for a keypress and abort(err_code)
global procedure wait_abort(sequence msg, integer err_code)
   wait(msg)
   abort(err_code)
end procedure

---------------------------------------------------------------------------------
-- ERROR ROUTINES --

-- force a fatal error and display the given msg
-- use this to force a debug dump to ex.err
global procedure fatal_error(sequence msg)
   crash_message("Error! : "&msg)
   ? 1/0
end procedure

integer error_rID    error_rID=routine_id("fatal_error")
global procedure error(sequence msg)
   call_proc(error_rID,{msg})
end procedure

global procedure set_error_handler(integer err_rID)
   if err_rID < 0 then error_rID = routine_id("fatal_error") end if
   error_rID = err_rID
end procedure


-- specify the error expr, if it's TRUE, call error(msg)
-- expr is a boolean
global procedure on_error(integer expr, sequence msg)
   if expr then error(msg) end if
end procedure

-- like check_error(), but passes expr through
global function check_error(integer expr, sequence msg)
   if expr then error(msg) end if
   return expr
end function

---------------------------------------------------------------------------------
-- EXCEPTION HANDLING ROUTINES --

-- list of exception rID's
-- {{ex_rID's},{ex_Data}}
sequence ex_stack    ex_stack={{},{}}
constant EX_rID   =1,
         EX_DATA  =2

-- this will be set to 1 if an exception handler is currently being processed
--   throw_exception() can only be called from an exception handler
integer ex_active    ex_active   =0
integer ex_pending   ex_pending  =0

-- setup ex_rID as a new exception handler
global function new_exception(integer ex_rID)
 integer f
   f = find(ex_rID,ex_stack[EX_rID])
   if f then
      return f
   else
      on_error((ex_rID = -1),"new_exception(): invalid routine_id\n")
      ex_stack[EX_rID] &=ex_rID
      ex_stack[EX_DATA]&={ {} }
      return length(ex_stack[EX_rID])
   end if
end function

-- call a given exception handler for ex_data
global procedure on_exception(object ex_data, integer ex_id)
   ex_active = ex_id
   ex_data = call_func(ex_stack[EX_rID][ex_id],{ex_data})
   ex_active = 0
end procedure

-- like on_exception(), except it passes ex_data through
global function check_exception(object ex_data, integer ex_id)
   ex_active = ex_id
   ex_data = call_func(ex_stack[EX_rID][ex_id],{ex_data})
   ex_active = 0
   return ex_data
end function

-- push ex onto the top of ex_stack for the active exception handler
global procedure throw_exception(object ex)
   on_error( not ex_active,
         "throw_exception(): called outside of an exception handler\n")

   ex_pending +=1
   ex_stack[EX_DATA][ex_active] &= { ex }
end procedure

-- check for a specific exception for the given handler
-- returns TRUE if specified exception is pending
global function catch_exception(integer ex_id, object ex_data)
   on_error( (ex_id < 1) or (ex_id > length(ex_stack[EX_rID])),
         "catch_exception(): invalid exception handler id\n")

   return find(ex_data, ex_stack[EX_DATA][ex_id])
end function

-- like catch_exception(), but clears the exception if caught
global function caught_exception(integer ex_id, object ex_data)
 integer found
   on_error( (ex_id < 1) or (ex_id > length(ex_stack[EX_rID])),
         "caught_exception(): invalid exception handler id\n")

   found = find(ex_data, ex_stack[EX_DATA][ex_id])
   if found then
      ex_pending -=1
      ex_stack[EX_DATA][ex_id] = splice(ex_stack[EX_DATA][ex_id],{found,found},"")
   end if
   return found
end function

-- return a list of all pending exceptions for the given exception id
global function get_exceptions(integer ex_id)
   on_error( (ex_id < 1) or (ex_id > length(ex_stack[EX_rID])),
         "get_exceptions(): invalid exception id\n")

   return ex_stack[EX_DATA][ex_id]
end function

-- clear all pending exceptions
global procedure clear_pending_exceptions(integer ex_id)
   on_error( (ex_id < 0) or (ex_id > length(ex_stack[EX_rID])),
         "clear_pending_exceptions(): invalid exception id\n")

   if ex_id then
      ex_pending -=length(ex_stack[EX_DATA][ex_id])
      ex_stack[EX_DATA][ex_id] = {}
   else
      ex_pending = 0
      for i = 1 to length(ex_stack[EX_DATA]) do
         ex_stack[EX_DATA][i] = {}         
      end for
   end if
end procedure

-- return the number of pending exceptions for a given ex_id
-- if ex_id = 0, then return total number of pending exceptions
global function pending_exceptions(integer ex_id)
   on_error( (ex_id < 0) or (ex_id > length(ex_stack[EX_rID])),
         "pending_exceptions(): invalid exception id\n")

   if ex_id = 0 then return ex_pending end if
   return length(ex_stack[EX_DATA][ex_id])
end function